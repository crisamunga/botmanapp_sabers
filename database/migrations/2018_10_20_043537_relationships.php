<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Relationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('house_id')->references('id')->on('houses')->onDelete('restrict');
        });

        Schema::table('houses', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('house_type_id')->references('id')->on('house_types')->onDelete('set null');
            $table->foreign('house_location_id')->references('id')->on('house_locations')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('houses', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['house_type_id']);
            $table->dropForeign(['house_location_id']);
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign(['house_id']);
            $table->dropForeign(['user_id']);
        });
    }
}