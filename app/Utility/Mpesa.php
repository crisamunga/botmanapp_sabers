<?php

namespace App\Utility;
use Registrar;
use STK;
use Log;

class Mpesa
{
    public static function register_urls()
    {
        $val = config('mpesa.accounts.staging.id_validation_callback');
        $conf = config('mpesa.accounts.staging.lnmo.callback');
        $shortcode = config('mpesa.accounts.staging.lnmo.shortcode');
        Registrar::submit($shortcode, $conf, $val);
    }

    public static function stk_push($amount, $number, $ref)
    {
        $account = config('mpesa.accounts.staging.lnmo.shortcode');
        STK::push($amount, $number, $ref, 'Payment for Sabers housing system');
    }
}
