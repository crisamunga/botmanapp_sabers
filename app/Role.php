<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUuid;

class Role extends \Spatie\Permission\Models\Role
{
    use HasUuid;
    public $incrementing = false;
}
