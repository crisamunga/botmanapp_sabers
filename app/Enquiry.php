<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUuid;

class Enquiry extends Model
{
    use HasUuid;
    public $incrementing = false;

    protected $fillable = ['email','phone','title','message'];

    public function getStateAttribute()
    {
        if($this->addressed) {
            return "Addressed";
        } else {
            return "Not Addressed";
        }
    }
}
