<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUuid;

class HouseWallet extends Model
{
    use HasUuid;
    public $incrementing = false;

    protected $fillable = ['monthly_plan'];

    public function getPaidAttribute()
    {
        $this->payments->sum('amount');
    }

    public function house() {
        return $this->belongsTo('App\House','house_id');
    }

    public function payments() {
        return $this->hasMany('App\Payment','house_wallet_id');
    }
}
