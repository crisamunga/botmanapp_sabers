<?php

namespace App\Http\Controllers;

use App\House;
use App\User;
use Illuminate\Http\Request;
use App\HouseLocation;
use App\HouseType;
use Validator;
use Log;

class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $houses = House::with(['house_location','house_type','user'])->get();
        return view('admin.houses.index')->with('houses', $houses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $houseTypes = HouseType::get();
        $houseLocations = HouseLocation::all();
        return view('admin.houses.create')->with('housetypes', $houseTypes)->with('houselocations', $houseLocations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required','string','max:15','unique:houses'],
            'cost' => ['required','numeric'],
            'house_type' => ['required','alpha_dash','exists:house_types,id'],
            'house_location' => ['required','alpha_dash','exists:house_locations,id'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        House::create([
            'name' => $request['name'],
            'cost' => $request['cost'],
            'house_type_id' => $request['house_type'],
            'house_location_id' => $request['house_location'],
        ]);

        return redirect()->route('admin.houses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $house = House::with(['user','payments'])->findOrFail($id);
        return view('admin.houses.show')->with('house', $house);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $houseTypes = HouseType::get();
        $houseLocations = HouseLocation::all();
        $house = House::with(['house_location','house_type'])->findOrFail($id);
        return view('admin.houses.edit')->with('house', $house)->with('housetypes', $houseTypes)->with('houselocations', $houseLocations);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $house = House::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => ['required','string','max:15','unique:houses,id,'.$house->id],
            'cost' => ['required','numeric'],
            'house_type' => ['required','alpha_dash','exists:house_types,id'],
            'house_location' => ['required','alpha_dash','exists:house_locations,id'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = [
            'name' => $request['name'],
            'cost' => $request['cost'],
            'house_type_id' => $request['house_type'],
            'house_location_id' => $request['house_location'],
        ];
        $input = array_filter($input, 'strlen');
        $house->fill($input)->save();

        return redirect()->route('admin.houses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $house = House::findOrFail($id);
        $house->delete();
        return redirect()->route('admin.houses.index');
    }

    public function edituser($id)
    {
        $house = House::with(['user'])->findOrFail($id);
        $users = User::all();
        return view('admin.houses.edituser')->with('house', $house)->with('users', $users);
    }

    public function updateuser(Request $request, $id)
    {
        $house = House::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'user' => ['nullable','alpha_dash','exists:users,id'],
            'monthly_plan' => ['required','numeric','min:50']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $input = $request['user'];
        Log::debug($input);
        if (isset($input) && $input != "none") {
            $user = User::findOrFail($input);
            $house->user()->associate($user);
            $house->monthly_plan = $request['monthly_plan'];
        } else {
            $house->user()->dissociate();
            $house->monthly_plan = 0.00;
        }
        $house->save();
        
        return redirect()->route('admin.houses.index');
    }
}
