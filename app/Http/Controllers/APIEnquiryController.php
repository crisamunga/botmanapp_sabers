<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use Log;
use App\Enquiry;

class APIEnquiryController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');
        
        $botman->listen();
    }

    public function make_enquiry(BotMan $bot)
    {
        $extras = $bot->getMessage()->getExtras();
        $apiReply = $extras['apiReply'];
        $apiAction = $extras['apiAction'];
        $apiIntent = $extras['apiIntent'];
        
        Log::debug($extras);
        
        $bot->reply("This is my reply");
    }

    public function enquiry(Request $request)
    {
        $qr = $request['queryResult'];
        $params = $request['queryResult']['parameters'];
        Enquiry::create([
            'title' => $qr['intent']['displayName'],
            'phone' => array_key_exists('phone',$params) ? $params['phone'] : null,
            'email' => array_key_exists('email',$params) ? $params['email'] : null,
            'message' => json_encode([
                'message' => $qr['queryText'],
                'details' => $params
            ])
        ]);
        return response()->json([
            "fulfillmentText" => "We'll get back to you on that",
            "source" => 'response',
            "payload" => [
                "google" => [
                    "expectUserResponse" => true,
                    "richResponse" => [
                      "items" => [
                            [
                                "simpleResponse" => [
                                    "textToSpeech" => "We'll get back to you on that"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ], 200);
    }
}
