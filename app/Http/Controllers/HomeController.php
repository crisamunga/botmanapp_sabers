<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\House;
use App\Payment;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $houses = Auth::user()->houses;
        $payments = Auth::user()->payments;
        $owner = Auth::user();
        return view('home')->with('houses',$houses)->with('payments',$payments)->with('owner',$owner);
    }
}
