<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Validator;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::all();

        return view('admin.admins.index')->with('admins', $admins);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required','string','max:255'],
            'email' => ['required','string','email','max:255','unique:admins'],
            'phone' => ['required','string','regex:/^((\+[0-9]{1,3})|0)[0-9]{9}$/','unique:admins,phone'],
            'national_id' => ['required','string','regex:/^[0-9]{8,12}$/','unique:admins,national_id'],
            'password' => ['required','string','min:6','confirmed']
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'national_id' => $request['national_id'],
            'password' => $request['password'],
        ]);

        return redirect()->route('admin.admins.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::findOrFail($id);
        
        return view('admin.admins.edit')->with('admin',$admin);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => ['required','string','max:255'],
            'email' => ['required','string','email','max:255','unique:admins,email,'.$admin->id],
            'phone' => ['required','string','regex:/^((\+[0-9]{1,3})|0)[0-9]{9}$/','unique:admins,phone,'.$admin->id],
            'national_id' => ['required','string','regex:/^[0-9]{8,12}$/','unique:admins,national_id,'.$admin->id]
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $input = $request->only(['name','email','phone','national_id','password']);
        $input = array_filter($input, 'strlen');
        $admin->fill($input)->save();

        return redirect()->route('admin.admins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Admin::findOrFail($id);
        $admin->delete();
        return redirect()->route('admin.admins.index');
    }
}
