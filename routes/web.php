<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/pay', 'PayController@index')->name('pay.index');
Route::post('/pay/complete', 'PayController@complete')->name('pay.complete');
Route::get('/pay/thankyou', 'PayController@thankyou')->name('pay.thankyou');

Route::any('/45cdfe', 'MPesaController@val')->name('mpesa.validate');
Route::any('/64cv98', 'MPesaController@con')->name('mpesa.confirm');

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('login.submit');

    Route::group(['middleware' => ['auth:admin']], function () {
        Route::get('/home', 'AdminHomeController@index')->name('home');
        Route::resource('admins', 'AdminController', ['except' => ['show']]);
        Route::resource('owners', 'UserController', ['except' => ['create','store','edit','update']]);
        Route::resource('houses', 'HouseController');
        Route::get('houses/{id}/edituser', 'HouseController@edituser')->name('houses.edituser');
        Route::put('houses/{id}/updateuser', 'HouseController@updateuser')->name('houses.updateuser');

        Route::resource('housetypes', 'HouseTypeController');
        Route::resource('houselocations', 'HouseLocationController');

        Route::get('/payments', 'PaymentController@index')->name('payments.index');
        Route::get('/payments/{id}', 'PaymentController@show')->name('payments.show');
        Route::post('payments/{id}/archive', 'PaymentController@archive')->name('payments.archive');;
        Route::post('payments/{id}/reverse', 'PaymentController@reverse')->name('payments.reverse');;
        Route::post('payments/{id}/confirm', 'PaymentController@confirm')->name('payments.confirm');;
        Route::post('payments/{id}/transfer', 'PaymentController@transfer')->name('payments.transfer');;

        Route::resource('enquiries', 'EnquiryController', ['only' => ['index','update']]);
    });
});