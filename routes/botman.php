<?php
use App\Http\Controllers\BotManController;
use BotMan\BotMan\Middleware\ApiAi;
use BotMan\BotMan\BotMan;
use App\Http\Controllers\APIEnquiryController;

$botman = resolve('botman');

$botman->hears('Hi', function ($bot) {
    $bot->reply('Hello!');
});
$botman->hears('Start conversation', BotManController::class.'@startConversation');


/**
 * Dialog flow stuff
 */
$dialogflow = ApiAi::create(config('dialogflow.key'))->listenForAction();
$botman->middleware->received($dialogflow);
$botman->hears('make_enquiry', APIEnquiryController::class.'@make_enquiry')->middleware($dialogflow);