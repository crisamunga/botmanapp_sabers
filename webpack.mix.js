let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(['resources/assets/js/bootstrap.js','resources/assets/js/bootstrap-notify.js'], 'public/js')
   .extract(['popper.js','jquery','datatables.net','datatables.net-dt'])
   .sass('resources/assets/sass/app.scss', 'public/css');
