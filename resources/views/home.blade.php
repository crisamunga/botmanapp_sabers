@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="container-fluid">

                        <div class="text-center">
                            <h2>{{$owner->name}}</h2>
                        </div>
                        <div class="row text-center">
                            <div class="col-sm">
                                <h5>Name</h5>
                                <p>{{$owner->name}}</p>
                            </div>
                            <div class="col-sm">
                                <h5>E-mail</h5>
                                <p>{{$owner->email}}</p>
                            </div>
                            <div class="col-sm">
                                <h5>Phone</h5>
                                <p>{{$owner->phone}}</p>
                            </div>
                            <div class="col-sm">
                                <h5>National id</h5>
                                <p>{{$owner->national_id}}</p>
                            </div>
                        </div>
                        <div>
                            <h3>Houses</h3>
                            <table id="house-list" class="table table-bordered table-hover datatable">
                                <thead class="thead-inverse|thead-default">
                                    <tr>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Location</th>
                                        <th>Initial cost</th>
                                        <th>Total paid</th>
                                        <th>Pending</th>
                                        <th>Monthly plan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($owner->houses as $house)
                                            <tr>
                                                <td>{{$house->name}}</td>
                                                <td>{{$house->type}}</td>
                                                <td>{{$house->location}}</td>
                                                <td>{{$house->cost}}</td>
                                                <td>{{$house->paid}}</td>
                                                <td>{{$house->pending}}</td>
                                                <td>{{$house->monthly_plan}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                        </div>

                        <div>
                            <h3>Payments</h3>
                            <table id="payment-list" class="table table-bordered table-hover datatable">
                                <thead class="thead-inverse|thead-default">
                                    <tr>
                                        <th>House</th>
                                        <th>Amount</th>
                                        <th>Mode</th>
                                        <th>Date</th>
                                        <th>State</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($owner->payments as $payment)
                                            <tr>
                                                <td>{{$payment->house->name}}</td>
                                                <td>{{$payment->amount}}</td>
                                                <td>{{$payment->mode}}</td>
                                                <td>{{$payment->created_at}}</td>
                                                <td>{{$payment->state}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

@section('footer')
    <script>
        $(function () {  
            $('#house-list').DataTable();
            $('#payment-list').DataTable();
        });
    </script>
@endsection