@extends('layouts.admin')

@section('content')
    <div class="text-center">
        <h3>Edit {{$houselocation->name}}</h3>
        <p>
            Fields marked by (*) are mandatory
        </p>
        @if ($errors->count() > 0)
            <p class="text-danger">
                It seems there were errors in your input
            </p>  
        @endif
    </div>

    <div>
        <form action="{{route('admin.houselocations.update',$houselocation)}}" method="POST" aria-label="{{ __('Edit house location') }}">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name',$houselocation->name) }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Update house location') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection