@extends('layouts.admin')

@section('content')

    <div class="text-center">
        <h2>{{$owner->name}}</h2>
    </div>
    <div class="row text-center">
        <div class="col-sm">
            <h5>Name</h5>
            <p>{{$owner->name}}</p>
        </div>
        <div class="col-sm">
            <h5>E-mail</h5>
            <p>{{$owner->email}}</p>
        </div>
        <div class="col-sm">
            <h5>Phone</h5>
            <p>{{$owner->phone}}</p>
        </div>
        <div class="col-sm">
            <h5>National id</h5>
            <p>{{$owner->national_id}}</p>
        </div>
    </div>
    <div>
        <h3>Houses</h3>
        <table id="house-list" class="table table-bordered table-hover datatable">
            <thead class="thead-inverse|thead-default">
                <tr>
                    <th>Name</th>
                    <th>Initial Cost</th>
                    <th>Pending</th>
                    <th>Monthly plan</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($owner->houses as $house)
                        <tr>
                            <td>
                                <a href="{{route('admin.houses.show',$house->id)}}">
                                    {{$house->name}}
                                </a>
                            </td>
                            <td>{{$house->cost}}</td>
                            <td>{{$house->pending}}</td>
                            <td>{{$house->monthly_plan}}</td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
    </div>

    <div>
        <h3>Payments</h3>
        <table id="payment-list" class="table table-bordered table-hover datatable">
            <thead class="thead-inverse|thead-default">
                <tr>
                    <th>House</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>State</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($owner->payments as $payment)
                        <tr>
                            <td>
                                <a href="{{route('admin.houses.show',$payment->house->id)}}">{{$payment->house->name}}</a>
                            </td>
                            <td>{{$payment->amount}}</td>
                            <td>{{$payment->created_at}}</td>
                            <td>{{$payment->state}}</td>
                            <td>
                                <a class="btn text-info bg-transparent" href="{{route('admin.payments.show',$payment->id)}}">
                                    <span class="mdi mdi-more mdi-24px"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
    </div>
@endsection

@section('footer')
    <script>
        $(function () {  
            $('#house-list').DataTable();
            $('#payment-list').DataTable();
        });
    </script>
@endsection