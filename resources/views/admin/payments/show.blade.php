@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-6">
            <div>
                <h4>Payment details</h4>
            </div>
            <table class="table table-bordered table-hover table-responsive">
                <tbody>
                    <tr>
                        <th>House</th>
                        <td>
                            <a href="{{route('admin.houses.show',$payment->house->id)}}">{{$payment->house->name}}</a>
                        </td>
                    </tr>
                    <tr>
                        <th>User</th>
                        <td>
                            <a href="{{route('admin.owners.show',$payment->user->id)}}">{{$payment->user->name}}</a>
                        </td>
                    </tr>
                    <tr>
                        <th>Amount</th>
                        <td>{{$payment->amount}}</td>
                    </tr>
                    <tr>
                        <th>Mode</th>
                        <td>{{$payment->mode}}</td>
                    </tr>
                    <tr>
                        <th>Date</th>
                        <td>{{$payment->created_at}}</td>
                    </tr>
                    <tr>
                        <th>State</th>
                        <td>{{$payment->state}}</td>
                    </tr>
                    <tr>
                        <td><b>Details</b></td>
                    </tr>
                    @foreach ($payment->details as $key => $description)
                        <tr>
                            <th>{{$key}}</th>
                            <td>{{$description}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="col-6">
            <div>
                <h4>Payment actions</h4>
            </div>
            @if (!$payment->archived)
                <form action="{{route('admin.payments.archive',$payment->id)}}" method="POST" class="my-5">
                    @csrf
    
                    <button class="btn btn-danger" type="submit">
                        <span class="mdi mdi-archive mdi-24px"></span> Archive
                    </button>
                </form>
            @endif

            @if (!$payment->reversed)
                <form action="{{route('admin.payments.reverse',$payment->id)}}" method="POST" class="my-5">
                    @csrf
    
                    <button class="btn btn-dark" type="submit">
                        <span class="mdi mdi-refresh mdi-24px"></span> Reverse
                    </button>
                </form>
            @endif
            
            @if (!$payment->confirmed)
                <form action="{{route('admin.payments.confirm',$payment->id)}}" method="POST" class="my-5">
                    @csrf
    
                    <button class="btn btn-primary" type="submit">
                        <span class="mdi mdi-check mdi-24px"></span> Confirm
                    </button>
                </form>
            @endif
            
            @if (!$payment->archived)
                <form action="{{route('admin.payments.transfer',$payment->id)}}" method="POST" class="my-5">
                    @csrf
                    <div class="form-group row mb-0">
                        <label for="house" class="col-md-8 offset-md-3">{{ __('Transfer payment to another house') }}</label>
                    </div>
    
                    <div class="form-group row">
                        <label for="house" class="col-md-4 col-form-label text-md-right">{{ __('House') }}</label>
        
                        <div class="col-md-6">
                            <input id="house" type="text" class="form-control{{ $errors->has('house') ? ' is-invalid' : '' }}" name="house" value="{{ old('house',$payment->house->house) }}" required autofocus>
        
                            @if ($errors->has('house'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('house') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
    
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-success">
                                <span class="mdi mdi-arrow-right mdi-24px"></span> Transfer
                            </button>
                        </div>
                    </div>
                </form>
            @endif
        </div>
    </div>
@endsection