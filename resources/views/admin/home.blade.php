@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Payments per month</div>

                <div class="card-body">
                    <div>
                        <table id="monthly_averages" class="table table-hover datatable">
                            <thead class="thead-default">
                                <tr>
                                    <th>Month</th>
                                    <th>Total</th>
                                    <th>Average</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($monthly_averages as $month => $averages)
                                        <tr>
                                            <td>{{$month}}</td>
                                            <td>{{$averages['total']}}</td>
                                            <td>{{$averages['average']}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-header">Payments per house</div>

                <div class="card-body">
                    <div>
                        <table id="house_averages" class="table table-hover datatable">
                            <thead class="thead-default">
                                <tr>
                                    <th>House</th>
                                    <th>Month</th>
                                    <th>Total</th>
                                    <th>Average</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($house_averages as $house => $monthaverages)
                                        @foreach ($monthaverages['values'] as $month => $averages)
                                            <tr>
                                                <td>
                                                    <a href="{{route('admin.houses.show',$house)}}">{{$monthaverages['house']}}</a>
                                                </td>
                                                <td>{{$month}}</td>
                                                <td>{{$averages['total']}}</td>
                                                <td>{{$averages['average']}}</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
    $(function () {  
        $('#monthly_averages').DataTable();
        $('#house_averages').DataTable({
            "order": [[ 0, 'asc' ]],
            "displayLength": 25,
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
    
                api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="3">'+group+'</td></tr>'
                        );
    
                        last = group;
                    }
                } );
            }
        });
    });
</script>
@endsection
