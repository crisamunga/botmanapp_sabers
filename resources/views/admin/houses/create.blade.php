@extends('layouts.admin')

@section('content')
    <div class="text-center">
        <h3>Add house</h3>
        <p>
            Fields marked by (*) are mandatory
        </p>
        @if ($errors->count() > 0)
            <p class="text-danger">
                It seems there were errors in your input
            </p>  
        @endif
    </div>

    <div>
        <form action="{{route('admin.houses.store')}}" method="POST" aria-label="{{ __('Add house location') }}">
            @csrf
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="cost" class="col-md-4 col-form-label text-md-right">{{ __('Cost (KSH)') }}</label>

                <div class="col-md-6">
                    <input id="cost" type="number" step="0.001" min="0" class="form-control{{ $errors->has('cost') ? ' is-invalid' : '' }}" name="cost" value="{{ old('cost') }}" required autofocus>

                    @if ($errors->has('cost'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('cost') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="house_type" class="col-md-4 col-form-label text-md-right">{{ __('House Type') }}</label>

                <div class="col-md-6">
                    <select name="house_type" id="house_type" class="form-control{{ $errors->has('house_type') ? ' is-invalid' : '' }}" required autofocus>
                        @foreach ($housetypes as $housetype)
                            <option value="{{$housetype->id}}">{{$housetype->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('house_type'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('house_type') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="house_location" class="col-md-4 col-form-label text-md-right">{{ __('House Location') }}</label>

                <div class="col-md-6">
                    <select name="house_location" id="house_location" class="form-control{{ $errors->has('house_location') ? ' is-invalid' : '' }}" required autofocus>
                        @foreach ($houselocations as $houselocation)
                            <option value="{{$houselocation->id}}">{{$houselocation->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('house_location'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('house_location') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Add house location') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection