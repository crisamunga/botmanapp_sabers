@extends('layouts.admin')

@section('content')
    <div class="text-center">
        <h3>{{$house->name}}</h3>
    </div>
    <div class="row">
        <h4>House details</h4>
    </div>

    <div class="row text-center">
        <div class="col-sm">
            <h5>Name</h5>
            <p>{{$house->name}}</p>
        </div>
        <div class="col-sm">
            <h5>Cost</h5>
            <p>{{$house->cost}}</p>
        </div>
        <div class="col-sm">
            <h5>Paid</h5>
            <p>{{$house->paid}}</p>
        </div>
        <div class="col-sm">
            <h5>Monthly plan</h5>
            <p>{{$house->monthly_plan}}</p>
        </div>
    </div>
    <div>
        <h4>Owner details</h4>
    </div>
    <div class="row text-center">
        @isset($house->user)
            <div class="col-sm">
                <h5>Name</h5>
                <p>{{$house->user->name}}</p>
            </div>
            <div class="col-sm">
                <h5>Email</h5>
                <p>{{$house->user->email}}</p>
            </div>
            <div class="col-sm">
                <h5>Phone</h5>
                <p>{{$house->user->phone}}</p>
            </div>
            <div class="col-sm">
                <h5>National id</h5>
                <p>{{$house->user->national_id}}</p>
            </div>
        @else
            <div class="col-sm">
                <h5>No owner set</h5>
            </div>
        @endisset
    </div>
    <div>
        <div>
            <h3>Payments</h3>
            <table id="payment-list" class="table table-bordered table-hover datatable">
                <thead class="thead-inverse|thead-default">
                    <tr>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>State</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($house->payments as $payment)
                            <tr>
                                <td>{{$payment->amount}}</td>
                                <td>{{$payment->created_at}}</td>
                                <td>{{$payment->state}}</td>
                                <td>
                                    <a class="btn text-info bg-transparent" href="{{route('admin.payments.show',$payment->id)}}">
                                        <span class="mdi mdi-more mdi-24px"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    @include('includes.datatables')
@endsection