<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'imaji') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css"> --}}

    <!-- Styles -->
    {{-- <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('mdi/css/materialdesignicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="row justify-content-md-center">
        <div class="col-md-2 mw-100 flex-grow-1 bg-dark sidebar collapse width show" id="admin-sidebar">
        {{-- <div class="col-md-3 float-left col-1 pl-0 pr-0 collapse show" id="admin-sidebar"> --}}
            <nav class="navbar navbar-dark navbar-static bg-dark flex-column">
                <a class="navbar-brand" href="{{ url('/admin/home') }}">
                    Admin
                </a>
                <!-- Links -->
                <ul class="navbar-nav admin-nav my-4">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('admin.home')}}"><span class="mdi mdi-view-dashboard mdi-24px"></span> Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.admins.index')}}"><span class="mdi mdi-account-key mdi-24px"></span>Admin users</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" href=""><span class="mdi mdi-account-multiple mdi-24px"></span>Admin groups</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=""><span class="mdi mdi-account-key mdi-24px"></span>Admin permissions</a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.owners.index')}}"><span class="mdi mdi-account mdi-24px"></span>Home owners</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.houses.index')}}"><span class="mdi mdi-home mdi-24px"> Houses</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.housetypes.index')}}"><span class="mdi mdi-home-modern mdi-24px"> House types</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.houselocations.index')}}"><span class="mdi mdi-home-map-marker mdi-24px"> House location</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.payments.index')}}"><span class="mdi mdi-credit-card mdi-24px"> Payments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.enquiries.index')}}"><span class="mdi mdi-comment-question mdi-24px"> Enquiries</a>
                    </li>
                </ul> 
            </nav>                      
        </div>
        <div class="col-md-10 m-0 p-0 mw-100 flex-grow-1 main">
        {{-- <div class="col-md-9 float-left col px-5 pl-md-2 pt-2 main mw-100 flex-grow-1 p-0"> --}}
            <nav class="navbar navbar-static navbar-expand-md navbar-dark bg-dark">
                <div class="container">
                    {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#admin-sidebar" aria-label="{{ __('Toggle sidebar') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button> --}}
                    <button class="btn btn-dark" data-toggle="collapse" data-target="#admin-sidebar">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>
    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                            @auth
                                <li class="nav-item">
                                    {{-- <a class="nav-link" href="{{route('home')}}">Home</a> --}}
                                </li>
                                <li class="nav-item">
                                    {{-- <a class="nav-link" href="{{route('map')}}">Map</a> --}}
                                </li>
                            @endauth
                        </ul>
    
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
    
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
    
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
    
            <main class="py-4 mx-5">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @yield('content')
            </main>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/bootstrap.js') }}"></script>
    {{-- <script src="{{ asset('js/jquery/jquery-3.3.1.min.js')}}"></script> --}}
    @yield('footer')

    <script>
        $(function(){
            $('ul.admin-nav li.active').removeClass('active');
            var pathname = window.location; 
            $('ul.admin-nav a[href="'+ pathname +'"]').parent().addClass('active');                
        });
    </script>
</body>
</html>